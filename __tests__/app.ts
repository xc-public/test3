import "reflect-metadata";
import express, {Express} from 'express';
import Config from "../server/xcomponents/config";
import Db from "../server/xcomponents/db";
import Services from "../server/xcomponents/services";
import Routers from "../server/xcomponents/routers";
import Resolvers from "../server/xcomponents/resolvers";
import Middlewares from "../server/xcomponents/middlewares";

export class App {

  public $router: Express;
  public $sv: Services;
  public $db: Db;
  public $config: any;
  public $mw: Middlewares;


  constructor() {
    this.$router = express();
  }

  public async boot(): Promise<void> {

    try {

      this.$config = await Config.make(this);
      this.$db = await Db.make(this);
      this.$sv = await Services.make(this);
      this.$mw = await Middlewares.make(this);
      await Routers.make(this);
      await Resolvers.make(this);


    } catch (e) {
      console.log(e)
    }
  }

}
