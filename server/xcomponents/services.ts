import {App} from "../app";
import glob from "glob";
import * as path from "path";
import AuthService from "../services/auth/auth.service";


export default class Services {

  public static async make(app: App): Promise<Services> {
    const services = new Services();

    try {

      const ignorePaths = [];
      for (const dbConfig of app.$config.dbs) {
        const srvPath = path.join(__dirname, `../services/${dbConfig.meta.dbAlias}/**/*.service.{js,ts}`);
        ignorePaths.push(srvPath);
        for (const servicePath of glob.sync(srvPath)) {
          const service = (await import(servicePath)).default;
          services[dbConfig.meta.dbAlias] = services[dbConfig.meta.dbAlias] || {};
          services[dbConfig.meta.dbAlias][service.name] = new service(app);
        }
      }


      for (const servicePath of glob.sync(path.join(__dirname, `../services/**/*.service.{js,ts}`), {ignore: ignorePaths})) {
        const service = (await import(servicePath)).default;
        services[service.name] = new service(app);
      }
    } catch (e) {
      console.log(e)
    }
    return Object.freeze(services);
  }


  public AuthService:AuthService;

}
