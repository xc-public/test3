import glob from "glob";
import * as path from "path";
import {App} from "../app";

import {graphqlHTTP} from 'express-graphql';
import {buildSchema, execute, GraphQLSchema} from 'graphql';
import {mergeResolvers, mergeTypeDefs} from '@graphql-tools/merge';


export default class Resolvers {

  public static async make(app: App): Promise<any> {


    console.log('api.init()');
    // @ts-ignore

    const types: string[] = [];
    // @ts-ignore
    const resolvers = [];

    try {

      for (const {meta: {dbAlias}} of app.$config.dbs) {


        const resolverPath = path.join(__dirname, '..', 'resolvers', dbAlias, '**', '*.resolver.{ts,js}');
        const typePath = path.join(__dirname, '..', 'resolvers', dbAlias, '**', '*.schema.{js,ts}');


        const resolverPaths = glob.sync(resolverPath);
        const schemaPaths = glob.sync(typePath);

        if (dbAlias === 'db') {
          resolverPaths.push(...glob.sync(path.join(__dirname, '..', 'resolvers', 'auth', '*.resolver.{ts,js}')));
          schemaPaths.push(...glob.sync(path.join(__dirname, '..', 'resolvers', 'auth', '*.schema.{ts,js}')));
        }


        for (const file of resolverPaths) {
          const resolver = (await import(file)).default
          const resolverInstance = new resolver(app);
          resolvers.push(resolverInstance.resolvers())
        }


        for (const file of schemaPaths) {
          const schemaType = (await import(file)).default
          types.push(schemaType)
        }

      }

      const rootValue = mergeResolvers(resolvers);

      this.applyGloabalMiddlewares(rootValue, app.$mw.$global);

      const schema: GraphQLSchema = buildSchema(
        mergeTypeDefs(types, {
          useSchemaDefinition: true,
          forceSchemaDefinition: true,
          throwOnConflict: true,
          commentDescriptions: true,
          reverseDirectives: true,
        })
      );


      // /**************** START : finally, load graphl schema ****************/
      app.$router.use('/graphql', (req, res, next) => graphqlHTTP({
        schema,
        rootValue,
        graphiql: {
          headerEditorEnabled: true
        },
        context: ({req, res, next}),
        async customExecuteFn(args): Promise<any> {
          return execute(args);
        }
      })(req, res));


    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  // @ts-ignore
  private static applyGloabalMiddlewares(resolvers: any, $global: any): any {
    if (!Object.keys($global).length) {
      return resolvers
    }

    for (const [name, resolver] of Object.entries(resolvers)) {
      resolvers[name] = async (...args) => {
        try {
          for (const handler of Object.values($global)) {
            await (handler as any).default(...args)
          }
          const result = await (resolver as any)(...args);
          return result;
        } catch (e) {
          console.log(e)
          throw e;
        }
      }
    }
    return resolvers
  }
}
