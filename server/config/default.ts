const config:object = {
  "gcp": {
    "cloudFunction": false
  },
  "aws": {
    "lambda": false
  },
  "azure": {
    "functionApp": false
  },
  "zeit": {
    "now": false
  },
  "alibaba": {
    "functionCompute": false
  },
  "serverlessFramework": {
    "express": false
  },
  "monitor": false
};

export default config;
