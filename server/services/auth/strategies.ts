import {promisify} from "util";
import passport from 'passport';
import bcrypt from 'bcryptjs';

import * as jwt from 'jsonwebtoken';
import {Strategy, ExtractJwt} from 'passport-jwt';
import {App} from "../../app";

const PassportLocalStrategy = require('passport-local').Strategy;

// todo : read from config
// const jwtOptions: any = {secretOrKey: 'dsdsdsds'}
// jwtOptions.jwtFromRequest = ExtractJwt.fromHeader('xc-auth');

passport.serializeUser(function ({id, email, email_verified, roles, provider, firstname, lastname}, done) {
  done(null, {
    id,
    email,
    email_verified,
    provider,
    firstname, lastname,
    roles: (roles || '')
      .split(',')
      .reduce((obj, role) => Object.assign(obj, {[role]: true}), {})
  })
  ;
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});


export function initStrategies(app: App) {


  passport.use(new Strategy({
    secretOrKey: app.$config.auth?.jwt?.secret,
    jwtFromRequest: ExtractJwt.fromHeader('xc-auth'),
    ...(app.$config?.auth?.jwt?.options || {})
  }, (jwt_payload, done) => {
    app.$db.XcUsers.findOne({
      email: jwt_payload?.email
    }).then(user => {
      if (user) {
        return done(null, user);
      } else {
        return done(new Error('User not found'));
      }
    }).catch(err => {
      return done(err);
    })
  }));


  passport.use(new PassportLocalStrategy({
      usernameField: 'email',
      session: false
    }, async function (email, password, done) {
      try {
        let user = await app.$db.XcUsers.findOne({email});
        if (!user) {
          return done({msg: `Email ${email} is not registered!`});
        }

        const hashedPassword = await promisify(bcrypt.hash)(password, user.salt);
        if (user.password !== hashedPassword) {
          return done({msg: `Password not valid!`});
        } else {
          return done(null, user);
        }
      } catch (e) {
        done(e);
      }
    }
  ));

}

