import {BaseResolver} from "xc-core-ts";
import {App} from "../../app";
import {promisify} from "util";

import * as jwt from 'jsonwebtoken';

const jwtOptions: any = {secretOrKey: 'dsdsdsds'};
import passport from 'passport';

export default class AuthResolver extends BaseResolver {

  private app: App;

  constructor(app: App) {
    super();
    this.app = app;
  }


  async signin(args, {req, res, next}) {
    req.body = args;
    return await this.app.$sv.AuthService.signin(req, res, next);
  }

  async signup(args, {req}) {
    let user = await this.app.$sv.AuthService.signup(args.data);
    await promisify((req as any).login.bind(req))(user);
    user = (req as any).user;
    return {
      token: jwt.sign({
        email: user.email,
        firstname: user.firstname,
        lastname: user.lastname,
        id: user.id,
        roles: user.roles
      }, jwtOptions.secretOrKey)
    };
  }

  async passwordForgot(args) {
    await this.app.$sv.AuthService.passwordForgot(args.email)
    return true;
  }


  async tokenValidate(args) {
    await this.app.$sv.AuthService.tokenValidate(args.tokenId);
    return true;
  }


  async passwordReset(args) {
    await this.app.$sv.AuthService.passwordReset(args.tokenId, args);
    return true
  }


  async passwordChange(args, {req}): Promise<any> {
    await this.app.$sv.AuthService.passwordChange(args, req);
    return true;
  }


  async emailVerification(args) {
    await this.app.$sv.AuthService.emailVerification(args.tokenId);
    return true;
  }


  async me(_args, {req}) {
    return req?.user ?? {};
  }


  async refreshToken(_args, {req, res}) {
    return await this.app.$sv.AuthService.generateJwtTokenByRefreshToken(req.cookies?.refresh_token, res);
  }

  async updateUser(args, {req}) {
    await this.app.$sv.AuthService.updateUser(req, args);
    return true;
  }


  public resolvers(): { [key: string]: (arg: any, ctx: any, arg3: any) => any } {
    this.app.$router.use(passport.initialize())
    // middleware for setting passport user( for treating non-authenticated user as guest)
    this.app.$router.use(async (req, res, next) => {
      const user = await new Promise(resolve => {
        passport.authenticate('jwt', {session: false}, (_err, user, _info) => {
          if (user) {
            return resolve(user);
          }
          resolve({roles: 'guest'})
        })(req, res, next);
      })

      await promisify((req as any).login.bind(req))(user);
      next();
    });


    return {
      SignIn: this.signin,
      SignUp: this.signup,
      Me: this.me,
      PasswordForgot: this.passwordForgot,
      PasswordReset: this.passwordReset,
      EmailValidate: this.emailVerification,
      TokenVerify: this.tokenValidate,
      ChangePassword: this.passwordChange,
      UpdateUser: this.updateUser,
      RefreshToken: this.refreshToken,
    }
  }


}
